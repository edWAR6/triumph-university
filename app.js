var express = require('express');
var path = require('path');
var app = express();

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.get('/', function(req, res){
  res.render('home.html');
});

app.get('/certificate', function(req, res){
  res.render('certificate.html');
});

app.listen(process.env.VCAP_APP_PORT || 3000);
